<?php

Route::get('/', 'SiteController@homepage');
Route::get('/register', 'SiteController@register');
Route::post('/postregister', 'SiteController@postregister');

Route::get('/login','AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('logout','AuthController@logout');

Route::group(['middleware' => ['auth', 'checkRole:admin']], function () {
	Route::get('/siswa', 'SiswaController@index');
	Route::post('/siswa/create', 'SiswaController@create');
	Route::get('/siswa/{siswa}/edit', 'SiswaController@edit');
	Route::post('/siswa/{siswa}/update', 'SiswaController@update');
	Route::get('/siswa/{siswa}/delete', 'SiswaController@delete');
	Route::get('/siswa/{siswa}/profile', 'SiswaController@profile');
	Route::post('/siswa/{id}/addnilai', 'SiswaController@addnilai');
	Route::get('/siswa/{id}/{idmapel}/deletenilai', 'SiswaController@deletenilai');
	Route::get('/siswa/exportexcel', 'SiswaController@exportExcel');
	Route::get('/siswa/exportpdf', 'SiswaController@exportPdf');
	Route::get('/guru/{id}/profile', 'GuruController@profile');
	Route::get('/posts', 'PostController@index')->name('posts.index');
	Route::get('/posts/{posts}/delete', 'PostController@delete');
	Route::get('/posts/{posts}/edit', 'PostController@edit');
	Route::post('/posts/{posts}/update', 'PostController@update');
	Route::get('post/add', [
		'uses' => 'PostController@add',
		'as' => 'posts.add'
	]);
	Route::post('post/create', [
		'uses' => 'PostController@create',
		'as' => 'posts.create'
	]);
});

Route::group(['middleware' => ['auth', 'checkRole:admin,siswa']], function () {
	Route::get('/dashboard', 'DashboardController@index')->middleware('auth');
});

Route::get('/{slug}', [
	'uses' => 'SiteController@singlepost',
	'as' => 'site.single.post'
]);