@extends('layouts.frontend')

@section('content')

<!-- Container -->
<div class="container">
	<!-- Info Panel -->
	<div class="row justify-content-center">
		<div class="col-9 0ffset-1 info-panel">
			<div class="row">
				<div class="col-4">
					<img src="{{asset('frontend/img/book.png')}}" class="float-left">
					<h4>Book</h4>
					<p>Lorem ipsum dolor sit amet</p>
				</div>
				<div class="col-4">
					<img src="{{asset('frontend/img/desk.png')}}" class="float-left">
					<h4>Classroom</h4>
					<p>Lorem ipsum dolor sit amet</p>
				</div>
				<div class="col-4">
					<img src="{{asset('frontend/img/graduate.png')}}" class="float-left">
					<h4>Education</h4>
					<p>Lorem ipsum dolor sit amet</p>
				</div>
			</div>
		</div>
	</div>
	<!-- End Info Panel -->
	<!-- Workingspace -->
	<section class="workingspace">
		<div class="row">
			<div class="col-lg">
				<img src="{{asset('frontend/img/library.jpg')}}" class="img-fluid">
			</div>
			<div class="col-lg">
				<h4>Best <span>facilities</span> for <span>learning</span></h4>
				<p>Learn new knowledge every day with complete supporting facilities</p>
				<a href="#" class="btn btn-primary tombol">Gallery</a>
			</div>
		</div>
	</section>
	<!-- End Workingspace -->

</div>
<!-- EndContainer -->
<!-- Start blog Area -->
<section class="news">
	<div class="container">			
		<div class="row mt-5 d-flex justify-content-center multiple-items">
			@foreach($posts as $post)
			<div class="col mb-5">
				<div class="thumb">
					<img class="img-fluid" src="{{$post->thumbnail()}}" alt="">								
				</div>
				<div class="caption">
					<p>{{$post->created_at->format('d M Y')}}  |  Oleh {{$post->user->name}}</p>
					<h6>{{$post->title}}</h6>
					<a href="{{route('site.single.post',$post->slug)}}"><span>Details </span><span class="lnr lnr-arrow-right"></span></a>
				</div>		
			</div>	
			@endforeach						
		</div>
	</div>	
</section>
<!-- End blog Area -->			
@endsection

@section('footer')
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
	$(document).ready(function(){
		$('.multiple-items').slick({
			slidesToShow: 3,
			slidesToScroll: 2
		});
	});
</script>
@stop