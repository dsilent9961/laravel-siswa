@extends('layouts.frontend')

@section('content')
<section class="news-detail mt-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-2">
				<p>{{$post->user->name}} </a> <span class="lnr lnr-user"></span></p>
				<p>{{$post->created_at->format('d M Y')}} </a> <span class="lnr lnr-calendar-full"></span></p>
				<div style="border-bottom: 2px solid black;"></div>
			</div>
			<div class="col-lg-7">
				<h3 class="text-center">{{$post->title}}</h3>
				<div class="box mt-4">
					<p style="text-align: justify;">{!!$post->content!!}</p>
				</div>
			</div>
		</div>
	</div>
</section>			  

@endsection