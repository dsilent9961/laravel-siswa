@extends('layouts.frontend')

@section('content')

<!-- Start search-course Area -->
<section class="newregister mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h4 class="display-4">Online Registration <br>
					<b>Join with Us</b>
				</h4>
				<p class="text-grey">An updated curriculum with market needs, We guarantee graduates will be creative people.</p>
			</div>
			<div class="col-lg-4 offset-2">
				<form action="/postregister" method="post">
					{{csrf_field()}}
					<div class="form-group{{$errors->has('nama') ? ' has-error' : ''}}">
						<label for="nama_lengkap">Full Name</label>
						<input type="text" class="form-control" name="nama" value="{{old('nama')}}">
						@if($errors->has('nama'))
						<span class="help-block">{{$errors->first('nama')}}</span>
						@endif
					</div> 

					<div class="form-group{{$errors->has('email') ? ' has-error' : ''}}">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" value="{{old('email')}}">
						@if($errors->has('email'))
						<span class="help-block">{{$errors->first('email')}}</span>
						@endif
					</div>

					<div class="form-group{{$errors->has('jenis_kelamin') ? ' has-error' : ''}}">
						<label for="jenis_kelamin">Gender</label>
						<select class="form-control" name="jenis_kelamin">
							<option selected>Choosee...</option>
							<option value="Laki laki"{{(old('jenis_kelamin') == 'Laki laki') ? ' selected' : ''}}>Laki laki</option>
							<option value="Perempuan"{{(old('jenis_kelamin') == 'Perempuan') ? ' selected' : ''}}>Perempuan</option>
						</select>
						@if($errors->has('jenis_kelamin'))
						<span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
						@endif
					</div>

					<div class="form-group{{$errors->has('agama') ? ' has-error' : ''}}">
						<label for="agama">Religion</label>
						<input type="text" class="form-control" name="agama" value="{{old('agama')}}">
						@if($errors->has('agama'))
						<span class="help-block">{{$errors->first('agama')}}</span>
						@endif
					</div>

					<div class="form-group">
						<label for="alamat">Address</label>
						<textarea class="form-control" rows="3" name="alamat">{{old('alamat')}}</textarea>
					</div>

					<button type="submit" class="btn btn-primary btn-block">Send</button>
				</form>
			</div>
		</div>
	</div>
</section>

@stop

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
	@if(Session::has('sukses'))
	toastr.success("{{Session::get('sukses')}}","Sukses")
	@endif
</script>
@stop