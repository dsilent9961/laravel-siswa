@extends('layouts.master')

@section('header')
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
@stop

@section('content')
<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-6">
					<div class="card shadow mb-4">
						<!-- Card Header - Dropdown -->
						<div class="card-header py-3 d-flex flex-row justify-content-center">
							<h6 class="m-0 font-weight-bold text-primary">Edit Data Siswa</h6>
						</div>
						<!-- Card Body -->
						<div class="card-body">
							<h6>Mata Pelajaran yang diajar oleh Guru {{$guru->nama}}</h6>
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Mata Pelajaran</th>
										<th>Semester</th>
									</tr>
								</thead>
								<tbody>
									@foreach($guru->mapel as $mapel)
									<tr>
										<td>{{$mapel->nama}}</td>
										<td>{{$mapel->semester}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@endsection