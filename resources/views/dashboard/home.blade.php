@extends('layouts.master')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xl-6 col-lg-7">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Data Siswa</h6>
				</div>
				<!-- Card Body -->
				<div class="card-body">
					<table class="table">
						<thead>
							<tr>
								<th>Ranking</th>
								<th>Nama</th>
								<th>Nilai</th>

							</tr>
						</thead>
						<tbody>
							@php
							$ranking = 1;
							@endphp
							@foreach(ranking5Besar() as $s)
							<tr>
								<td>{{$ranking}}</td>
								<td>{{$s->nama}}</td>
								<td>{{$s->rataRataNilai}}</td>
							</tr>
							@php
							$ranking++;
							@endphp
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-xl-3 col-md-3 mb-4">
			<div class="card border-left-success shadow h-40 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Guru</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800">{{totalGuru()}}</div>
						</div>
						<div class="col-auto">
							<i class="fas fa-user fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-3 col-md-3 mb-4">
			<div class="card border-left-primary shadow h-40 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Siswa</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800">{{totalSiswa()}}</div>
						</div>
						<div class="col-auto">
							<i class="fas fa-users fa-2x text-gray-300"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection