@extends('layouts.master')

@section('header')
<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendor/editable/css/bootstrap-editable.css')}}">
@stop

@section('content')
<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			@if(session('sukses'))
			<div class="alert alert-success" role="alert">
				Data Berhasil Di Input
			</div>

			@endif
			@if(session('error'))
			<div class="alert alert-danger" role="alert">
				Data Mata Pelajaran sudah ada
			</div>
			@endif
			<div class="row">
				<div class="col-md-4 col-xs-12">	
					<div class="card shadow mb-4">
						<div class="card-header bg-white py-3">
							<h6 class="m-0 font-weight-bold">Detail Siswa
								<span><a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm float-right">Edit Profile</a></span></h6>
							</div>
							<div class="card-body">
								<div class="form-inline mb-3">
									<h5 class="font-weight-bold">{{$siswa->nama}}</h5>
									<img src="{{$siswa->getAvatar()}}" class="rounded-circle shadow ml-auto" width="60px" height="70px" alt="Avatar">
								</div>
								<h4 class="small">Jenis Kelamin<span class="float-right">{{$siswa->jenis_kelamin}}</span></h4>
								<h4 class="small">Agama <span class="float-right">{{$siswa->agama}}</span></h4>
								<h4 class="small">Alamat <span class="float-right">{{$siswa->alamat}}</span></h4>
							</div>
							<div class="form-inline small justify-content-center mb-3 text-center">
								<div class="card bg-primary text-white shadow">
									<div class="card-body">
										<h5><b>{{$siswa->mapel->count()}}</b></h5>
										<div class="text-white-50 small">Mata Pelajaran</div>
									</div>
								</div>
								<div class="card bg-warning text-white shadow">
									<div class="card-body">
										<h5><b>217</b></h5>
										<div class="text-white-50 small">Point Siswa</div>
									</div>
								</div>
								<div class="card bg-success text-white shadow">
									<div class="card-body">
										<h5><b>{{$siswa->rataRataNilai()}}</b></h5>
										<div class="text-white-50 small">Rata-rata Nilai</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8 col-xs-12">
						<div class="card shadow mb-4">
							<div class="card-header bg-white py-3">
								<h6 class="m-0 font-weight-bold text-primary">Mata Pelajaran <span>
									<button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">Tambah Nilai</button></span>
								</h6>
							</div>
							<div class="card-body">
								<table class="table">
									<thead>
										<tr>
											<th>Kode</th>
											<th>Nama</th>
											<th>Semester</th>
											<th>Nilai</th>
											<th>Guru</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										@foreach($siswa->mapel as $mapel)
										<tr>
											<td>{{$mapel->kode}}</td>
											<td>{{$mapel->nama}}</td>
											<td>{{$mapel->semester}}</td>
											<td>
												<a href="#" class="nilai" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilai" data-title="Masukkan">{{$mapel->pivot->nilai}}</a>
											</td>
											<td><a href="/guru/{{$mapel->guru_id}}/profile">{{$mapel->guru->nama}}</a></td>
											<td>
												<a href="/siswa/{{$siswa->id}}/{{$mapel->id}}/deletenilai" class="btn btn-danger btn-sm" onclick="return confirm('Yakin di Hapus?')"><i class="fas fa-trash"></i></a></td>
											</td>

										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="card ml-4">
						<div class="card-body shadow">
							<div id="chartNilai"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="judulModal">Tambah Nilai</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="/siswa/{{$siswa->id}}/addnilai" method="POST">
						{{csrf_field()}}
						<div class="form-group">
							<label for="mata_pelajaran">Mata Pelajaran</label>
							<select class="form-control" name="mapel">
								@foreach($matapelajaran as $mp)
								<option value="{{$mp->id}}">{{$mp->nama}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group{{$errors->has('nilai') ? ' has-error' : ''}}">
							<label for="nama_lengkap">Nilai</label>
							<input type="text" class="form-control" name="nilai" value="{{old('nilai')}}">
							@if($errors->has('nilai'))
							<span class="help-block">{{$errors->first('nilai')}}</span>
							@endif
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div> 
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection

	@section('footer')
	<script type="text/javascript" src="{{asset('admin/assets/vendor/editable/js/bootstrap-editable.js')}}"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script>
		Highcharts.chart('chartNilai', {
			chart: {
				type: 'column'
			},
			title: {
				text: 'Laporan Nilai Siswa'
			},
			xAxis: {
				categories: {!!json_encode($categories)!!},
				crosshair: true
			},
			yAxis: {
				min: 0,
				title: {
					text: 'nilai'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Nilai',
				data: {!!json_encode($data)!!}

			}]
		});

		$(document).ready(function() {
			$('.nilai').editable();
		});
	</script>
	@endsection