@extends('layouts.master')

@section('content')
<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 mb-4">
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus"></i> Tambah Siswa</button>
        </div>
        <div class="col-xl-12 col-lg-12">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header bg-white py-3">
              <h6 class="font-weight-bold text-primary">Data Siswa
                <span class="float-right">
                  <a href="/siswa/exportexcel" class="btn btn-primary btn-sm float-right">Export Excel</a>
                  <a href="/siswa/exportpdf" class="btn btn-primary btn-sm float-right mr-1">Export Pdf</a>
                </span></h6>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Agama</th>
                        <th>Alamat</th>
                        <th>Rata2 Nilai</th>
                        <th>Aksi</th>   
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data_siswa as $siswa)
                      <tr>
                        <td><a class="text-dark" href="/siswa/{{$siswa->id}}/profile">{{$siswa->nama}}</a></td>
                        <td>{{$siswa->jenis_kelamin}}</td>
                        <td>{{$siswa->agama}}</td>
                        <td>{{$siswa->alamat}}</td>
                        <td>{{$siswa->rataRataNilai()}}</td>
                        <td><a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                          <a href="#" class="btn btn-danger btn-sm delete" siswa-id="{{$siswa->id}}"><i class="fas fa-trash"></i></a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Siswa</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="/siswa/create" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group{{$errors->has('nama') ? ' has-error' : ''}}">
                  <label for="nama_lengkap">Nama Legkap</label>
                  <input type="text" class="form-control" name="nama" value="{{old('nama')}}">
                  @if($errors->has('nama'))
                  <span class="help-block">{{$errors->first('nama')}}</span>
                  @endif
                </div> 

                <div class="form-group{{$errors->has('email') ? ' has-error' : ''}}">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" name="email" value="{{old('email')}}">
                  @if($errors->has('email'))
                  <span class="help-block">{{$errors->first('email')}}</span>
                  @endif
                </div>

                <div class="form-group{{$errors->has('jenis_kelamin') ? ' has-error' : ''}}">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <select class="form-control" name="jenis_kelamin">
                    <option selected>Pilih...</option>
                    <option value="Laki laki"{{(old('jenis_kelamin') == 'Laki laki') ? ' selected' : ''}}>Laki laki</option>
                    <option value="Perempuan"{{(old('jenis_kelamin') == 'Perempuan') ? ' selected' : ''}}>Perempuan</option>
                  </select>
                  @if($errors->has('jenis_kelamin'))
                  <span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
                  @endif
                </div>

                <div class="form-group{{$errors->has('agama') ? ' has-error' : ''}}">
                  <label for="agama">Agama</label>
                  <input type="text" class="form-control" name="agama" value="{{old('agama')}}">
                  @if($errors->has('agama'))
                  <span class="help-block">{{$errors->first('agama')}}</span>
                  @endif
                </div>

                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" rows="2" name="alamat">{{old('alamat')}}</textarea>
                </div>

                <div class="form-group{{$errors->has('avatar') ? ' has-error' : ''}}">
                  <label for="avatar">Avatar</label>
                  <input type="file" class="form-control" name="avatar">
                  @if($errors->has('avatar'))
                  <span class="help-block">{{$errors->first('avatar')}}</span>
                  @endif
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      @stop

      @section('footer')
      <script>
        $('.delete').click(function(){
          var siswa_id = $(this).attr('siswa-id');
          swal({
            title: "Yakin?",
            text: "Mau di hapus siswa dengan id "+siswa_id+" ??",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              window.location = "/siswa/"+siswa_id+"/delete";
            }
          });
        });
      </script>
      <!-- Page level plugins -->
      <script src="{{asset('admin/assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('admin/assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

      <!-- Page level custom scripts -->
      <script src="{{asset('admin/assets/js/demo/datatables-demo.js')}}"></script>
      @stop