@extends('layouts.master')

@section('content')
<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
          Data Berhasil Di Input
        </div>
        @endif
        <div class="col-lg-5">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row justify-content-center">
              <h6 class="m-0 font-weight-bold text-primary">Edit Data Siswa</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <form action="/siswa/{{$siswa->id}}/update" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                  <label for="nama_lengkap">Nama Legkap</label>
                  <input type="text" class="form-control" name="nama" value="{{$siswa->nama}}">
                </div>
                <div class="form-group">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <select class="form-control" name="jenis_kelamin">
                    <option value="Laki laki" @if($siswa->jenis_kelamin == 'Laki laki') selected @endif>Laki laki</option>
                    <option value="Perempuan" @if($siswa->jenis_kelamin == 'Perempuan') selected @endif>Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="agama">Agama</label>
                  <input type="text" class="form-control" name="agama" value="{{$siswa->agama}}">
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea class="form-control" rows="4" name="alamat">{{$siswa->alamat}}</textarea>
                </div>
                <div class="form-group">
                  <label for="avatar">Avatar</label>
                  <input type="file" class="form-control" name="avatar">
                </div>
                <button type="submit" class="btn btn-warning btn-block mt-5">Update</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection