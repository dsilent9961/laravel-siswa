<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	@if(auth()->user()->role == 'admin')

	<!-- Nav Item - Dashboard -->
	<li class="nav-item">
		<a class="nav-link" href="/dashboard">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
		</li>

		<!-- Nav Item - Charts -->
		<li class="nav-item">
			<a class="nav-link" href="/siswa">
				<i class="fas fa-fw fa-user"></i>
				<span>Siswa</span></a>
			</li>

			<!-- Nav Item - Tables -->
			<li class="nav-item">
				<a class="nav-link" href="/posts">
					<i class="fas fa-fw fa-table"></i>
					<span>Post</span></a>
				</li>

				<!-- Divider -->
				<hr class="sidebar-divider d-none d-md-block">

				<!-- Sidebar Toggler (Sidebar) -->
				<div class="text-center d-none d-md-inline mb-auto">
					<button class="rounded-circle border-0" id="sidebarToggle"></button>
				</div>
				@endif

			</ul>
    <!-- End of Sidebar -->