<!doctype html>
<html lang="en">

<head>
	<title>Admin</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{asset('admin/assets/vendor/fontawesome-free/css/all.min.css')}}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{asset('admin/assets/css/sb-admin-2.css')}}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- TOASTR -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	<!-- DATATABLES -->
	<link href="{{asset('admin/assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

	<style>
		.ck-editor__editable {
			min-height: 300px;
		}
	</style>

	@yield('header')
</head>

<body id="page-top">
	<!-- WRAPPER -->
	<div id="wrapper">
		@include('layouts.includes._sidebar')
		@include('layouts.includes._navbar')
		<!-- MAIN -->
		@yield('content')
		<!-- END MAIN -->
		@include('layouts.includes._bottom')
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	
	<script src="{{asset('admin/assets/vendor/jquery/jquery.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
	<script src="{{asset('admin/assets/js/sb-admin-2.min.js')}}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<script src="{{asset('frontend/js/ckeditor.js')}}"></script>
	<script>
		@if(Session::has('sukses'))
		toastr.success("{{Session::get('sukses')}}","Sukses")
		@endif
	</script>
	<script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
	<script>
		ClassicEditor
		.create( document.querySelector( '#content1' ))
		.catch( error => {
			console.error(error);
		});
		$(document).ready(function(){
			$('#lfm').filemanager('image');
		});
	</script>
	@yield('footer')

</body>

</html>
