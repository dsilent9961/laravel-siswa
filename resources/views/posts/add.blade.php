@extends('layouts.master')

@section('content')
<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header">
              <h6 class="m-0 font-weight-bold text-primary">Tambah Berita</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <form action="{{route('posts.create')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group{{$errors->has('title') ? ' has-error' : ''}}">
                  <label for="title_lengkap">Title</label>
                  <input type="text" class="form-control" name="title" value="{{old('title')}}">
                  @if($errors->has('title'))
                  <span class="help-block">{{$errors->first('title')}}</span>
                  @endif
                </div> 
                <div class="form-group">
                  <label for="alamat">Content</label>
                  <textarea class="form-control" rows="4" id="content1" name="content">{{old('content')}}</textarea>
                </div>
                <div class="input-group my-5">
                 <span class="input-group-btn">
                   <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-secondary text-white">
                    <i class="fas fa-fw fa-file-image"></i> Choose
                  </a>
                </span>
                <input id="thumbnail" class="form-control" type="text" name="thumbnail" value="Select thumbnail...">
              </div>
              <img id="holder" style="margin-top:50px; max-height:100px;">
              <button type="submit" class="btn btn-primary btn-block">Tambah Post</button>
            </form>
          </div>
        </div>
      </div>
      <div>
      </div>
    </div>
  </div>
</div>
</div>
@stop
