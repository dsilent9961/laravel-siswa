@extends('layouts.master')

@section('content')
<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6">
          <form class="form-inline navbar-search">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <div class="input-group">
                <input type="text" class="form-control" name="cari" placeholder="Cari" aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="submit">
                    <i class="fas fa-search fa-sm"></i>
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-lg-6">
          <a href="{{route('posts.add')}}" class="btn btn-primary shadow-sm float-right">Add new post</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-12 col-lg-12">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Data Berita</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <table class="table">
                <thead>
                 <tr>
                  <th>Title</th>
                  <th>User</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @php $id=1; @endphp
                @foreach($posts as $post)
                <tr>
                  <td>{{$id++}}</td>
                  <td>{{$post->title}}</td>
                  <td>{{$post->user->name}}</td>
                  <td><a target="_blank" href="{{route('site.single.post', $post->slug)}}" class="btn btn-info btn-sm">View</a>
                    <a href="/posts/{{$post->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <a href="#" class="btn btn-danger btn-sm delete delete" posts-id="{{$post->id}}">Hapus</a></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>


@stop

@section('footer')
<script>
  $('.delete').click(function(){
    var posts_id = $(this).attr('posts-id');
    swal({
      title: "Yakin?",
      text: "Mau di hapus siswa dengan id "+posts_id+" ??",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location = "/posts/"+posts_id+"/delete";
      }
    });
  });
</script>
@stop