@extends('layouts.master')

@section('content')
<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
          Data Berhasil Di Input
        </div>
        @endif
        <div class="col-lg-8">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row justify-content-center">
              <h6 class="m-0 font-weight-bold text-primary">Edit Post</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <form action="/posts/{{$posts->id}}/update" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group{{$errors->has('title') ? ' has-error' : ''}}">
                  <label for="title_lengkap">Title</label>
                  <input type="text" class="form-control" name="title" value="{{$posts->title}}">
                  @if($errors->has('title'))
                  <span class="help-block">{{$errors->first('title')}}</span>
                  @endif
                </div> 
                <div class="form-group">
                  <label for="alamat">Content</label>
                  <textarea class="form-control" rows="4" id="content1" name="content">{{$posts->content}}</textarea>
                </div>
                <div class="input-group my-5">
                 <span class="input-group-btn">
                   <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-secondary text-white">
                    <i class="fas fa-fw fa-file-image"></i> Choose
                  </a>
                </span>
                <input id="thumbnail" class="form-control" type="text" name="thumbnail" value="{{$posts->thumbnail}}">
              </div>
              <img id="holder" style="margin-top:50px; max-height:100px;">
              <button type="submit" class="btn btn-primary btn-block">Update Post</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection

