<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
	public function index(Request $request)
	{
		if($request->has('cari')){
			$posts = \App\Post::where('title','LIKE','%' .$request->cari. '%')->get();
		}else{
			$posts = \App\Post::all();
		}
		return view('posts.index', compact('posts'));
	}

	public function add()
	{
		return view('posts.add');
	}

	public function create(Request $request)
	{
		$post = Post::create([
			'title' => $request->title,
			'content' => $request->content,
			'user_id' => auth()->user()->id,
			'thumbnail' => $request->thumbnail
		]);

		return redirect()->route('posts.index')->with('sukses','Post berhasil di submit');
	}

	public function delete(Post $posts)
	{
		$posts->delete($posts);
		return redirect('/posts')->with('sukses', 'Data berhasil dihapus');
	}

	public function edit(Post $posts)
	{
		return view('/posts/edit', compact('posts'));
	}

	public function update(Request $request, Post $posts)
	{

		$posts->update($request->all());
		if($request->hasFile('thumbnail')){
            $request->thumbnail->move('images/',$request->thumbnail->getClientOriginalName());
            $posts->thumbnail = $request->thumbnail->getClientOriginalName();
            $posts->save();
        }

		return redirect('/posts')->with('sukses','Data Berhasil diinput');
	}
}
