<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Post;	

class SiteController extends Controller
{
	public function homepage()
	{		
		$posts = Post::all();
		return view('sites.homepage', compact(['posts']));
	}

	public function register()
	{
		return view('sites.register');
	}

	public function postregister(Request $request)
	{
		// Insert user table
		$user = new \App\User;
		$user->role = 'siswa';
		$user->name = $request->nama;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		$user->remember_token = Str::random(60);
		$user->save();

         // Insert siswa table
		$request->request->add(['user_id' => $user->id]);
		$siswa = \App\Siswa::create($request->all());

		return redirect('/register')->with('sukses','Data pendaftar berhasil di masukkan');
	}

	public function singlepost($slug)
	{
		$post = Post::where('slug','=',$slug)->first();
		return view('sites.singlepost',compact(['post']));
	}
}
